package com.agiletestingalliance;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DurationTest {

    @Test
    public void durationTest() {
        String expected = "CP-DOF is designed specifically for corporates and working professionals alike. If you are a corporate and can't dedicate full day for training, then you can opt for either half days course or  full days programs which is followed by theory and practical exams.";
        Duration underTest = new Duration();
        assertEquals("test Duration dur()", expected, underTest.dur());

    }
}
