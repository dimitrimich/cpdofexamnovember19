package com.agiletestingalliance;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MinMaxTest {
    @Test
    public void functionMinMaxTestLarger() {
        final int expected = 10;
        final MinMax underTest = new MinMax();
        assertEquals("functionMinMaxTestLarger", expected, underTest.functionMinMax(5, 10));
    }
    @Test
    public void functionMinMaxTestSmaller() {
        final int expected = 10;
        final MinMax underTest = new MinMax();
        assertEquals("functionMinMaxTestLarger", expected, underTest.functionMinMax(10, 5));
    }
}
